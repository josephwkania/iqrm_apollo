IQRM-APOLLO: A C++ implementation of the Inter-Quartile Range Mitigation (IQRM) algorithm to remove Radio Frequency Interference.

Dependencies:
Needs boost libraries > 1.55.0

Installation:
The code uses cmake 2.0 and above for compilation. Here is an example below:

` git clone https://gitlab.com/kmrajwade/iqrm_apollo.git`


` cd iqrm_apollo; mkdir build; cd build`


` cmake -DBOOST_ROOT=<top level path to boost install> ../`


` make -j 16`

This will create an executable called `iqrm_apollo_cli`. Currently the code takes number of time samples as input. As long as the user provides
all time samples in the filterbank or a divisor of the total time samples in the file, the whole file will be processed. One can process any number 
of time samples at a time but that can lead to a variable channels being masked in the output filterbank file. At the moment, the flagged channels 
are replaced with the median of the data but more options will be made available to the user in a later update. 

Usage:

` iqrm_apollo_cli -m 3 -t 6.0 -i /local2/J2043+2740/58470_80654_J2043+2740_000004.fil -s 234375 -f 672 -o test.fil`



