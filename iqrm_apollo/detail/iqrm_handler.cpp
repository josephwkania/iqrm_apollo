#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cassert>
#include <new>
#include "boost/algorithm/string.hpp"
#include "iqrm_apollo/common.hpp"
#include "iqrm_apollo/raw_bytes.hpp"
#include "iqrm_apollo/iqrm_handler.hpp"

#define HANDLE_ERROR(x) \
{ \
	cudaError_t err = x; \
	if( cudaSuccess != err ) \
	{ \
		fprintf( stderr, \
		         "CUDA Error on call \"%s\": %s\n\tLine: %d, File: %s\n", \
		         #x, cudaGetErrorString( err ), __LINE__, __FILE__); \
		fflush( stdout );\
		exit( 1 ); \
	} \
}

namespace detail
{
template<typename T>
static inline double Lerp(T v0, T v1, T t)
{
    return (1 - t)*v0 + t*v1;
}

template<typename T>
static inline std::vector<T> Quantile(const std::vector<T>& inData, const std::vector<T>& probs)
{
    if (inData.empty())
    {
        return std::vector<T>();
    }

    if (1 == inData.size())
    {
        return std::vector<T>(1, inData[0]);
    }

    std::vector<T> data = inData;
    std::sort(data.begin(), data.end());
    std::vector<T> quantiles;

    for (size_t i = 0; i < probs.size(); ++i)
    {
        T poi = Lerp<T>(-0.5, data.size() - 0.5, probs[i]);

        size_t left = std::max(int64_t(std::floor(poi)), int64_t(0));
        size_t right = std::min(int64_t(std::ceil(poi)), int64_t(data.size() - 1));

        T datLeft = data.at(left);
        T datRight = data.at(right);

        T quantile = Lerp<T>(datLeft, datRight, poi - left);

        quantiles.push_back(quantile);
    }

    return quantiles;
}

template<typename NumericalRepType>
void iqrmcpu_cpu_kernel(iqrm_apollo::RawBytes<NumericalRepType>& block, std::size_t tsamples, std::size_t nchans, std::vector<float>& results, std::vector<float>& means, float& median)
{


    for (std::size_t jj=0; jj < tsamples; ++jj)
    {
	for (std::size_t ii=0; ii < nchans; ++ii)
	{
		results[ii] += ((float)(*(block.ptr() + ii + jj*nchans)) * (float)(*(block.ptr()+ ii + jj*nchans))/(float)(tsamples));
                means[ii] += ((float)(*(block.ptr() + ii + jj*nchans))/(float)tsamples);
	}
    
    }
    for (std::size_t ii=0; ii < nchans; ++ii)
    {
	    results[ii] = results[ii] - (means[ii] * means[ii]);
    }
   
    median = detail::Quantile<float>(means, {0.25,0.5,0.75})[1];
}
}

using namespace iqrm_apollo;

template<typename NumRepType, typename HandlerType>
IqrmHandler<NumRepType, HandlerType>::IqrmHandler(HandlerType& handler, std::size_t tsamples, std::size_t nchans, std::size_t maxlags, float nsigma)
: _handler(handler)
, _tsamples(tsamples)
, _nchans(nchans)
, _max_lag(maxlags)
, _nsigma(nsigma)
{
}

template<typename NumRepType, typename HandlerType>
IqrmHandler<NumRepType, HandlerType>::~IqrmHandler()
{
}

template<typename NumRepType, typename HandlerType>
void IqrmHandler<NumRepType, HandlerType>::init(RawBytes<char>& block)
{
    _handler.init(block);
}

template<typename NumRepType, typename HandlerType>
void IqrmHandler<NumRepType, HandlerType>::operator()(RawBytes<NumRepType>& block)
{   
  
    try
    {
	    // Allocate host memory
	    std::vector<float> results(_nchans,0);
	    std::vector<float> means(_nchans,0);
            float median;

	    BOOST_LOG_TRIVIAL(info) << "Running IQRM Algorithm....";

	    // compute per channel std for the TF block
	    detail::iqrmcpu_cpu_kernel<NumRepType>(block, _tsamples, _nchans, results, means, median);

	    //std::vector<float> original_data = results;

	    // Compute IQR range
	    std::vector<bool> flags(results.size(), false);
	    std::vector<bool> flags_per_lag(results.size(), false);
	    std::vector<float> diff_vector(results.size());
	    //std::vector<float> original_data = results;

	    for (std::size_t ii= 0; ii<= 2*_max_lag; ++ii)
	    {

		    //Rotate the vector
		    std::size_t jj = 0;

		    if (ii <= _max_lag)
		    {
			    std::size_t ind=0;
			    for (std::size_t kk=0; kk < results.size(); ++kk)
			    {
				    if (kk <= results.size() -( _max_lag - ii) - 1)
					    diff_vector[kk] = std::sqrt(results[kk]) - std::sqrt(results[kk + (_max_lag - ii)]);
				    else
				    {
					    diff_vector[kk] = std::sqrt(results[kk]) - std::sqrt(results[ind]);
					    ++ind;
				    }
			    }

		    }
		    else
		    {
			    std::size_t ind=0;
			    for (std::size_t kk=0; kk < results.size(); ++kk)
			    {
				    if (kk < ii)
				    {
					    diff_vector[kk] = std::sqrt(results[kk]) - std::sqrt(results[results.size() - 1 -ind]);
					    ++ind;
				    }
				    else
					    diff_vector[kk] = std::sqrt(results[kk]) - std::sqrt(results[kk - ii]);
			    }
		    }


		    std::vector<float> quant = detail::Quantile<float>(diff_vector, {0.25, 0.5, 0.75});
                    BOOST_LOG_TRIVIAL(info) << "Quantiles are: " << quant[0] << " " << quant[2] << " " << quant[2]; 
		    // Generate a difference vector for different lags
		    float std_dev = std::abs((quant[2] - quant[0]) / 1.349);

		    std::generate(flags_per_lag.begin(), flags_per_lag.end(), [&] () {bool flag = std::abs(diff_vector[jj] - quant[1]) > _nsigma*std_dev;
				    ++jj;
				    return flag; });

		    //Logical OR on the an initial mask
		    for (std::size_t kk=0; kk < flags.size(); ++kk)
		    {
			    flags[kk] = flags_per_lag[kk] | flags[kk];
		    }

		    //return host vector back to normal

	    }

	    // Write new flags via the adapter
	    float fraction = 0.0;
	    for (std::size_t ii=0; ii < flags.size(); ++ii)
	    {
		    if (flags[ii] == true )
		    {
			for (std::size_t jj=0; jj< _tsamples; ++jj)
			{
			    *(block.ptr() + ii + (jj * _nchans)) =  median;
			}
                        ++fraction;
		    }
	    }
            BOOST_LOG_TRIVIAL(info) << "Fraction of channels flagged: " << (fraction/(float)_nchans);
	    BOOST_LOG_TRIVIAL(info) << "IQRM finished";
    }

    catch (...)
    {
        throw;
    }

    _handler(block);
}

