#include "iqrm_apollo/simple_file_writer.hpp"

namespace iqrm_apollo {

    template<typename NumRepType>
    void SimpleFileWriter::operator()(RawBytes<NumRepType>& block)
    {
        try
        {
            BOOST_LOG_TRIVIAL(info) << "Writing " << block.total_bytes()*sizeof(NumRepType) << " bytes to file";
            _outfile.write(reinterpret_cast<const char*>(block.ptr()), block.total_bytes()*sizeof(NumRepType));
            BOOST_LOG_TRIVIAL(info) << "Written " << block.total_bytes()*sizeof(NumRepType) << " bytes to file";
        }
        catch (...)
        {
            throw;
        }
    }

} //psrdada_cpp
