#include "iqrm_apollo/sigprocheader.hpp"
#include <string>
#include <cstring>


namespace iqrm_apollo
{
    
    template<typename NumericT>
    void SigprocHeader::header_write(char*& ptr, std::string const& name, NumericT val)
    {
        header_write(ptr,name);
        std::memcpy(ptr,(char*)&val,sizeof(val));
        ptr += sizeof(val);
    }

} // namespace psrdada_cpp 
