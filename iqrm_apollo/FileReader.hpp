#ifndef IQRM_APOLLO_FILEREADER_HPP
#define IQRM_APOLLO_FILEREADER_HPP


#pragma once

#include "iqrm_apollo/raw_bytes.hpp"
#include "iqrm_apollo/sigprocheader.hpp"
#include "iqrm_apollo/common.hpp"
#include <ostream>
#include <string>
#include <iostream>
#include <fstream>

// A templated binary SIGPROC format file reader


namespace iqrm_apollo
{

template<typename NumRepType, typename HandlerType>
class FileReader
{
public:
	FileReader(std::string fileName, HandlerType& handler, std::size_t nelements); // create array based on file with this name
	~FileReader(void);
	bool getStatus(void); // true means "good", false means "bad"
	char getItem(int index); // get item at position specified by index

	// proper use requires properly invoking the destructor
	NumRepType& operator[](int index); // overload for both get and set

	bool read(RawBytes<NumRepType>& block, std::size_t& nelements);

	std::streampos	fileSize(void);

	void start();

	void stop();


private:
    HandlerType& _handler;
    std::size_t _nelements;
    std::size_t _written_bytes;
    std::size_t _file_size;
    FilHead _header;
    bool _stop;
    bool _running;
    std::ifstream _file; // stream to/from array file
    bool _status; // file status



	// enable use of operator[] to set values in array
	NumRepType getValue(int index); // get value from file

	NumRepType _value; // operator[] returns reference to here


	// these variables support use of operator[] on the left side of an
	// assignment statement.
	// this requires proper invocation of the destructor, to deal with prevIndex
	int _prevIndex;
	NumRepType _prevValue;

};
}
#include "iqrm_apollo/detail/FileReader.cpp"
#endif
