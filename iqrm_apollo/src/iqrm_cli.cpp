#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <string>
#include "boost/algorithm/string.hpp"
#include "iqrm_apollo/FileReader.hpp"
#include "iqrm_apollo/simple_file_writer.hpp"
#include "iqrm_apollo/iqrm_handler.hpp"
#include "boost/program_options.hpp"

namespace
{
  const size_t ERROR_IN_COMMAND_LINE = 1;
  const size_t SUCCESS = 0;
  const size_t ERROR_UNHANDLED_EXCEPTION = 2; 
}


using namespace iqrm_apollo;

int main(int argc, char* argv[])
{
    try
    {
        std::string size_string;
        std::string outfilename;
        std::string infilename;
        std::size_t tsamples;
        std::size_t maxlags;
        float nsigma;
        std::size_t nchans;

        /**
         * Define and parse the program options
         */
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
            ("help,h", "Print help messages")

            ("maxlags,m", po::value<std::size_t>(&maxlags)->default_value(3),
             "Maximum number of lags to compute the difference in standard deviations")

            ("nsigma,t", po::value<float>(&nsigma)->default_value(3.0),
             "Threshold in units of std for filtering")

            ("output,o", po::value<std::string>(&outfilename)->required(),
             "Name of the output file")

            ("input,i", po::value<std::string> (&infilename)->required(),
             "Name of the input file")

            ("samples,s", po::value<std::size_t> (&tsamples)->required(),
             "Number of time samples to process")

	    ("channels,f", po::value<std::size_t> (&nchans)->required(),
             "Number of frequency channels in the file");




        /* Catch Error and program description */
        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "IQRM-Apollo: Inter-Quartile Range Mitigation algorithm for narrow-band RFI"
                    << std::endl << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch(po::error& e)

        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        
        // CLI app over here
        auto nelements = nchans * tsamples;

        SimpleFileWriter writer(outfilename);

	IqrmHandler<uint8_t, decltype(writer)> iqrm(writer, tsamples, nchans, maxlags, nsigma);

        FileReader<uint8_t, decltype(iqrm)> reader(infilename, iqrm, nelements);

        reader.start();

        return(0);

    }
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
            << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;

}
