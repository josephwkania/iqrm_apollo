#include "iqrm_apollo/simple_file_writer.hpp"
#include <string>
#include <sstream>

namespace iqrm_apollo {

    SimpleFileWriter::SimpleFileWriter(std::string filename)
    {
        _outfile.open(filename.c_str(),std::ifstream::out | std::ifstream::binary);
        if (_outfile.is_open())
        {
            BOOST_LOG_TRIVIAL(debug) << "Opened file " << filename;
        }
        else
        {
            std::stringstream stream;
            stream << "Could not open file " << filename;
            throw std::runtime_error(stream.str().c_str());
        }
    }

    SimpleFileWriter::~SimpleFileWriter()
    {
        _outfile.close();
    }

    void SimpleFileWriter::init(RawBytes<char>& block)
    {
        _outfile.write(block.ptr(), block.total_bytes());
    }

    void SimpleFileWriter::init(RawBytes<char>& block, std::size_t size)
    {
        _outfile.write(block.ptr(), size);
    }

} //psrdada_cpp
