#ifndef IQRM_APOLLO_SIMPLE_FILE_WRITER_HPP
#define IQRM_APOLLO_SIMPLE_FILE_WRITER_HPP

#include "iqrm_apollo/common.hpp"
#include "iqrm_apollo/raw_bytes.hpp"
#include <fstream>
#include <iomanip>

namespace iqrm_apollo {

    class SimpleFileWriter
    {
        public:
            explicit SimpleFileWriter(std::string filename);
            SimpleFileWriter(SimpleFileWriter const&) = delete;
            ~SimpleFileWriter();
            void init(RawBytes<char>&);
            void init(RawBytes<char>&, std::size_t);

            template<typename NumRepType>
            void operator()(RawBytes<NumRepType>& block );

        private:
            std::ofstream _outfile;
    };
}//psrdada_cpp
#include "iqrm_apollo/detail/simple_file_writer.cpp"
#endif //TB_TRANSPOSE_SIMPLE_FILE_WRITER_HPP
