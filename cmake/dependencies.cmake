#include(cuda)
include(compiler_settings)
include(cmake/boost.cmake)
include_directories(SYSTEM ${Boost_INCLUDE_DIR})
set(DEPENDENCY_LIBRARIES
    ${Boost_LIBRARIES}
    ${CUDA_CUDART_LIBRARY}
)
